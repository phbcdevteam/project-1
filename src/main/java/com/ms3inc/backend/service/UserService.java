package com.ms3inc.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms3inc.backend.dao.UserDao;
import com.ms3inc.backend.domain.User;

@Service
public class UserService {

	@Autowired
	private UserDao userDao;
	
	public User getUserByUsername(String username) {
		return userDao.getUserByUsername(username);
	}
	
	
}
