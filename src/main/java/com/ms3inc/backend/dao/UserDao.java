package com.ms3inc.backend.dao;

import com.ms3inc.backend.domain.User;

public interface UserDao {

	/**
	 * getUserByUsername(String)
	 * retrieves User object by username
	 * @param username
	 * @return User
	 */
	public User getUserByUsername(String username);
	
	/**
	 * getUserById(int)
	 * retrieves User object by id
	 * @param id
	 * @return User
	 */
	public User getUserById(int id);
	
	/**
	 * isUser(int)
	 * Overload
	 * Checks if the user exist using id then returns true, false otherwise.
	 * @param id
	 * @return boolean
	 */
	public boolean isUser(int id);
	
	/**
	 * isUser(String)
	 * Overload
	 * Checks if the user exist using username then returns true, false otherwise.
	 * @param username
	 * @return boolean
	 */
	public boolean isUser(String username);
}
