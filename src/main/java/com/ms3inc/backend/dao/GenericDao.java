package com.ms3inc.backend.dao;

import javax.sql.DataSource;

public interface GenericDao {

	/**
	 * setDataSource(javax.sql.DataSource)
	 * Set the data-source that will be required 
	 * to create a connection to the database.
	 * @param ds
	 */
	public void setDataSource(DataSource ds);
	
	/**
	 * create(Object)
	 * Create a record in a table
	 * @param obj
	 * @return boolean
	 */
	public boolean create(Object obj);
	
	/**
	 * delete(Object)
	 * Delete a specific object from the table
	 * @param param
	 * @return boolean
	 */
	public boolean delete(Object obj);
	
	/**
	 * update(Object)
	 * Update a specific object from the table
	 * @param org
	 * @return boolean
	 */
	public boolean update(Object obj);
	
	/**
	 * 
	 */
	public void cleanup();
}
