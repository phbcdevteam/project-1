package com.ms3inc.backend.daoimpl;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.ms3inc.backend.dao.GenericDao;
import com.ms3inc.backend.dao.UserDao;
import com.ms3inc.backend.domain.User;

@Repository
public class UserDaoImpl implements GenericDao, UserDao {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	@Autowired
	public void setDataSource(DataSource ds) {
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(ds);
	}
	
	@Override
	public boolean create(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean delete(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean update(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public User getUserByUsername(String username) {
		SqlParameterSource param = new MapSqlParameterSource("USERNAME", username);
		String sqlQuery = "SELECT id, username, password FROM user where username = :USERNAME";
		
		User user = namedParameterJdbcTemplate.queryForObject(sqlQuery, param, new UserRowMapper());
		
		return user;
	}

	@Override
	public User getUserById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUser(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isUser(String username) {
		// TODO Auto-generated method stub
		return false;
	}
	

}
