package com.ms3inc.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ms3inc.backend.domain.User;
import com.ms3inc.backend.service.UserService;

@RestController
@RequestMapping("login")
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping
	public String index() {
		return "login index";
	}
	
	@RequestMapping(value="/getUser/{username}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public User getJson(@PathVariable String username){
		return userService.getUserByUsername(username);
	}
}
